﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Calculator
{
    /// <summary>
    /// Commands for Arithmetic operation
    /// </summary>
    public static class CustomCommand
    {
        static RoutedUICommand addition=new RoutedUICommand("Call addiotion method","Add",typeof(CustomCommand));
        public static RoutedUICommand Add { get { return addition; } }

        static RoutedUICommand subtraction = new RoutedUICommand("Call Subtraction method", "Sub", typeof(CustomCommand));
        public static RoutedUICommand Sub { get { return subtraction; } }

        static RoutedUICommand multiplication= new RoutedUICommand("Call Multiplication method", "mul", typeof(CustomCommand));
        public static RoutedUICommand Mul { get { return multiplication; } }

        static RoutedUICommand division = new RoutedUICommand("Call Division method", "Div", typeof(CustomCommand));
        public static RoutedUICommand Div { get { return division; } }
    }
}
