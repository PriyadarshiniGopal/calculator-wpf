﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // fields to get input value
        double num1, num2;
        Arithmetic.ArithmeticOperations calc;
        public MainWindow()
        {
            calc = new Arithmetic.ArithmeticOperations();
            InitializeComponent();
        }

        /// <summary>
        /// Add two number and set result
        /// </summary>
        /// <param name="sender">reference to the element</param>
        /// <param name="e">event data</param>
        public void Addition(object sender, ExecutedRoutedEventArgs e)
        {
            double ans = calc.Add(num1, num2);
            result.Text = String.Format("{0:0.00}", ans);
        }

        /// <summary>
        /// Subtract second number from first number and set result
        /// </summary>
        /// <param name="sender">reference to the element</param>
        /// <param name="e">event data</param>
        public void Subtraction(object sender, ExecutedRoutedEventArgs e)
        {
            double ans = calc.Subtract(num1, num2);
            result.Text = String.Format("{0:0.00}", ans);
        }

        /// <summary>
        /// Multiply two numbers and set result
        /// </summary>
        /// <param name="sender">reference to the element</param>
        /// <param name="e">event data</param>
        public void Multiplication(object sender, ExecutedRoutedEventArgs e)
        {
            double ans = calc.Multiply(num1, num2);
            result.Text = String.Format("{0:0.00}", ans);
        }

        /// <summary>
        /// divide first number ny second number and set result
        /// </summary>
        /// <param name="sender">reference to the element</param>
        /// <param name="e">event data</param>
        public void Division(object sender, ExecutedRoutedEventArgs e)
        {
            double ans = calc.Divide(num1, num2);
            result.Text = String.Format("{0:0.00}", ans);
        }

        public void CheckInput(Object sender,CanExecuteRoutedEventArgs e)
        {
            try
            {
                num1 = Convert.ToDouble(number1.Text);
                num2 = Convert.ToDouble(number2.Text);
                e.CanExecute = true;
            }
            catch
            {
                e.CanExecute = false;
            }
        }
    }
}
